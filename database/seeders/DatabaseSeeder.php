<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $users = \App\Models\User::factory()->count(3)->create();
        \App\Models\Article::factory()
            ->count(3)
            ->for($users->random())
            ->has(\App\Models\Category::factory()->count(rand(1, 3)), 'categories')
            ->create();
    }
}
